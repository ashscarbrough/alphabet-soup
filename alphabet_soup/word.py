"""Word Module to be used in crossword list creation"""
class Word:
    """Class to create objects of type Word with associated functions"""
    def __init__(self, word):
        """
        Constructor for the creation of a Word object.
        Args:
            self: Object of class Word.
            word (str): A string representation of a designated word.
        """
        self.word = word
        self.length = len(word)
        self.found = False
        self.start = None
        self.end = None

    def set_found(self, found):
        """
        Mutator function to update the found property.
        Args:
            self: Object of class Word.
            found (bool): Value to represent if the word is found in the crossword grid.
        """
        self.found = found

    def set_start(self, row, column):
        """
        Mutator function to update the starting location of the grid array.
        Args:
            self: Object of class Word.
            row (int): The index of the row location that the starting letter is found.
            column (int): The index of the column location that the starting letter is found.
        """
        self.start = f"{row}:{column}"

    def set_end(self, row, column):
        """
        Mutator function to update the end location of the grid array.
        Args:
            self: Object of class Word.
            row (int): The index of the row location that the final letter of the word.
            column (int): The index of the column location that the final letter of the word.
        """
        self.end = f"{row}:{column}"

    def get_found(self):
        """
        Accessor function to get the value of the found property
        Args:
            self: Object of class Word.
        Returns:
            found (bool): Found property indicating whether a word has been found or not.
        """
        return self.found

    def get_length(self):
        """
        Accessor function to get the length of the word.
        Args:
            self: Object of class Word.
        Returns:
            length (int): Value indicating the length of the word.
        """
        return self.length

    def __str__(self):
        """
        String function to provide the desired string output of the word.
        Args:
            self: Object of class Word.
        Returns:
            string (str): String output with format: {word} {starting-location} {ending-location}.
        """
        return f"{self.word} {self.start} {self.end}"
    