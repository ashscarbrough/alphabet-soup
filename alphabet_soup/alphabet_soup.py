"""Module used to parse crossword input and identify word locations"""
import sys
import os
import re
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from alphabet_soup.word import Word
from alphabet_soup.grid import Grid

def file_input(file_name):
    """
    Receive input from file.
    Args:
        file_name (str): Name of the file containing input for processing word puzzle.
    Returns:
        file_content (str): String representation of the content of the file.
    """
    # Try to read from file - General error information provided
    try:
        with open(file_name, "r", encoding='ascii') as file:
            file_content = file.read()

        return file_content
    except OSError:
        print("An error occurred while attempting to read the specified file name:", file_name)
        sys.exit()

def validate_dimensions(grid_dimensions):
    """
    Evaluate the dimension strings with a regular expression to determine if dimensions are valid.
    Args:
        dimensions (str): String taken from file content to define crossword puzzle grid dimensions.
    Returns:
        valid (bool): A boolean value to indicate whether the provided dimensions are valid.
    """
    pattern = "^[1-9]\d*[x][1-9]\d*$"
    return re.match(pattern, grid_dimensions)

def parse_dimensions(dimension_line):
    """
    Parse the dimensions string provided by the file input.
    Args:
        dimension_line (str): String taken from file content to define crossword grid dimensions.
    Returns:
        row_dimension (int): Integer value for number of rows of crossword grid.
        column_dimension (int): Integer value for  number of columns of crossword grid.
    """
    dimensions_list = dimension_line.split("x")

    return int(dimensions_list[0]), int(dimensions_list[1])

def parse_grid(file_content, grid_rows):
    """
    Parse the file input to obtain the crossword grid matrix.
    Args:
        file_content (str): String representation of the content of the file.
        rows (int): Integer value for the number of rows of the crossword grid.
    Returns:
        grid (list(list(string/characters))): A matrix of the crossword puzzel characters.
    """
    grid = []
    line = 1  # Start processing grid on line following dimensions

    while line <= grid_rows:
        stripped_line = file_content[line].strip()
        grid_line = stripped_line.split()
        grid.append(grid_line)
        line += 1

    return grid

def parse_wordlist(file_content, grid_rows, file_total_rows):
    """
    Print each word from the word list along with its associated start and end points on the grid.
    Args:
        file_content (str): String representation of the content of the file.
        rows (int): Integer value for the number of rows of the crossword puzzle grid.
        file_total_rows (int): Integer for the total number of rows of the file content.
    Returns:
        word_list (list(str)): A list of the words to find in the wordsearch grid.
    """
    word_object_list = []

    # Start processing word list after the dimensions line (1) & the rows comprising grid.
    line = 1 + grid_rows  
    while line < file_total_rows:
        stripped_line = (file_content[line].strip()).replace(" ", "")
        word_object_list.append(Word(stripped_line))  # Create a Word object for each word
        line += 1

    return word_object_list

def search_grid(grid_rows, grid_columns, grid, word_object_list):
    """
    Iterate each value in grid to find a matching letter, then search a direction for matches. 
    Args:
        rows (int): The total number of rows in the grid.
        columns (int): The total number of columns in the grid.
        gird (list(list(string))): The matrix of characters obtain from an input file.
        word_list (list(Word objects)): The list of words to look for within the grid.
    """
    # Search the crossword grid, row by row, and column by column for matches with words
    for row in range (grid_rows):
        for column in range (grid_columns):

            for word_object in word_object_list:
                # At each start location search for char matches for each word's start letter.
                # If a letter is matched then search each direction.
                if  word_object.get_found() is False and grid[row][column] == word_object.word[0]:
                    word_length = word_object.get_length()

                    # Search up-left - if statements eliminate collisions with grid boundaries
                    if word_length <= (row + 1) and word_length <= (column + 1):
                        search_direction(row, column, word_object,
                                         grid, row_increment=-1, column_increment=-1, word_index=1)

                    # Search up
                    if word_length <= (row + 1):
                        search_direction(row, column, word_object,
                                         grid, row_increment=-1, column_increment=0, word_index=1)

                    # Search up-right
                    if word_length <= (row + 1) and word_length <= (grid_columns - column):
                        search_direction(row, column, word_object,
                                        grid, row_increment=-1, column_increment=1, word_index=1)

                    # Search left
                    if word_length <= (column + 1):
                        search_direction(row, column, word_object,
                                         grid, row_increment=0, column_increment=-1, word_index=1)

                    # Search right
                    if word_length <= (grid_columns - column):
                        search_direction(row, column, word_object,
                                         grid, row_increment=0, column_increment=1, word_index=1)

                    # Search down-left
                    if word_length <= (column + 1) and word_length <= (grid_rows - row):
                        search_direction(row, column, word_object,
                                         grid, row_increment=1, column_increment=-1, word_index=1)

                    # Search down
                    if word_length <= (grid_rows - row):
                        search_direction(row, column, word_object,
                                         grid, row_increment=1, column_increment=0, word_index=1)

                    # Search down-right
                    if word_length <= (grid_columns - column) and word_length <= (grid_rows - row):
                        search_direction(row, column, word_object,
                                         grid, row_increment=1, column_increment=1, word_index=1)

def search_direction(start_row, start_column, word_object,
                     grid, row_increment, column_increment, word_index):
    """
    Recursive function to search for character matches between a word and a grid.
    Args:
        start_row (int): The row location of the word starting point.
        start_column (int): The column location of the word starting point.
        word (Word): Object of type Word .
        grid (Grid): An object of the class Grid that holds data on the grid that will be searched.
        row_increment (int): The directional increment of the rows during a search.
        column_increment (int): The directional increment of the columns during a search.
        word_index (int): Positional location a letter in a word that will be searched in the grid.
    """
    if grid[start_row + (row_increment * word_index)][start_column + (
        column_increment * word_index)] == word_object.word[word_index]:
        if word_index + 1 == word_object.get_length():
            word_object.set_start(start_row, start_column)
            word_object.set_end(start_row + (row_increment * word_index), start_column + (
                column_increment * word_index))
            word_object.set_found(True)
        else:
            search_direction(start_row, start_column, word_object,
                             grid, row_increment, column_increment, word_index + 1)
    else:
        return

if __name__ == '__main__':
    # Main Function
    # Get system argument from command line/terminal for file input
    FILE_NAME = sys.argv[1]

    # Obtain file content from provided file name and split them for parsing
    text_content = file_input(FILE_NAME)
    content_lines = text_content.split("\n")
    dimensions = content_lines[0]

    try:
        # Validate dimensions to ensure crossword grid can be parsed
        if validate_dimensions(dimensions):
            # Parse the dimension string, grid, and word list found in the file content
            rows, columns = parse_dimensions(dimensions)

            matrix_grid = parse_grid(content_lines, rows)
            crossword_grid = Grid(columns, rows, matrix_grid)

            word_list = parse_wordlist(content_lines, rows, len(content_lines))

            search_grid(rows, columns, crossword_grid.get_grid(), word_list)

            for word in word_list:
                print(word)

        else:
            print("Provided grid dimensions are invalid.  Dimensions must be in format: 5x5")

    except IndexError:
        print("An Index error occurred while processing the provided input file.")
        print("Ensure the format of the input file is aligned with the specifications in README.md")

    except Exception as e:
        # handle any other exception
        print(f"Error occured. Arguments: {e.args}.")
