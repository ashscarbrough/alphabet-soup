"""Grid Module to be used in crossword processing."""
class Grid:
    """Class to create an object of type Grid with associated functions"""
    def __init__(self, row_dimension, column_dimension, grid_matrix):
        """
        Constructor for the creation of the crossword grid made from a matrix.
        Args:
            self (Grid): Object of class Grid.
            row_dimension (int): The integer length of the crossword grid.
            column_dimension (int): The integer depth of the crossword grid.
            gird_matrix (list(list(string)): The matrix of characters obtain from an input file.
        """
        self.row_dimension = row_dimension
        self.column_dimension = column_dimension
        self.grid = grid_matrix

    def get_dimensions(self):
        """
        Accessor to get the values for dimension_x and dimension_y.
        Args:
            self (Grid): Object of class Grid.
        Returns:
            row_dimension (int): The integer for the number of rows in the crossword grid.
            column_dimension (int): The integer for the number of columns in the crossword grid.
        """
        return self.row_dimension, self.column_dimension

    def get_grid(self):
        """
        Accessor to get the grid matrix.
        Args:
            self (Grid): Object of class Grid.
        Returns:
            gird(list(list(string))): The matrix of characters obtain from an input file.
        """
        return self.grid
    