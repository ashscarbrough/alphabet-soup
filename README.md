# Alphabet Soup

> You have been contracted from a newspaper tasked with the job of providing an answer key to their word search for the Sunday print. The newspaper's word search is a traditional game consisting of a grid of characters in which a selection of words have been hidden. You are provided with the list of words that have been hidden and must find the words within the grid of characters. 

## Requirements
Load a character grid with scrambled words embedded within it and a words list of the words to find.  The following conditions apply:

- Within the grid of characters, the words may appear vertical, horizontal or diagonal.
- Within the grid of characters, the words may appear forwards or backwards. 
- Words that have spaces in them will not include spaces when hidden in the grid of characters.

### Input Format
The program is to accept a file as input. The file is an ASCII text file containing the word search board along with the words that need to be found. 

The file contains three parts. The first part is the first line, and specifies the number of rows and columns in the grid of characters, separated by an 'x'. The second part provides the grid of characters in the word search. The third part in the file specifies the words to be found.

The first line indicates how many following lines in the file contain the rows of characters that make up the word search grid. Each row in the word search grid will have the specified number of columns of characters, each separated with a space. The remaining lines in the file specify the words to be found.

The file format is as follows:

```
3x3
A B C
D E F
G H I
ABC
AEI
```

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

### Output Format
The output will specify the word found, along with the indices specifying where the beginning and ending characters of the word are located in the grid. A single space character will separate the word from the beginning and ending indices. The order of the words in the output should remain the same as the order of the words specified in the input file. The program will output to screen or console (and not to a file). 

The word search grid row and column numbers can be used to identify the location of individual characters in the board. For example, row 0 column 0 (represented as `0:0`) is the top-left character of a 3x3 board.  The bottom-right corner of a 3x3 board would be represented as `2:2`.

```
ABC 0:0 0:2
AEI 0:0 2:2
```

## Sample Data
The following may be used as sample input and output datasets.

### Input

```
5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE
```

### Ouput

```
HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1
```

==========================================

# Python Version
3.10.10

# PIP Version
23.1.2

# Assumptions
1. Inputs will be provided via a general .txt file with the following consistent structure:
    a. Dimension Line: Grid dimensions on line 1, Grid Lines: lines will have a grid based on dimensions proposed in the first line, Word Lines: The remaining lines of the file (with no additional empty lines) will be comprised of a list of words to search.
    b. Crossword Grid Size (passed as a string with the following structure): "int(rows) x int(columns)", example "10x10".
    b. Based on the size of the proposed crossword grid (provided in line 1), with the first number representing rows and the second representing the number of grid columns. 
    c. The remaining lines of the input will be comprised of the words that need to be located in the crossword grid.
    d. All alphabetic characters found in the grid and word bank will be capitalized.
    e. All provided words will be actual English words, and will not exceed 999 characters.
    f. Words will not appear in the crossword grid more than once.
2. Outputs will be provided in the following format:
    a. WORD starting-location - int(array index x): int(array index y) ending-location - int(array index x): int(array index y)

# Functional Requirements
1. FR 1 - Application should be able to load string input from a .txt file.
2. FR 2 - Application should be able to parse the grid dimensions
3. FR 3 - Application should be able to parse the character grid based on the provided grid dimensions string.
4. FR 4 - Application should be able to build a list of words from the provided input file.
5. FR 5 - The application must be able to find words in the grid vertically, horizontally, or diagonally.
6. FR 6 - The application must be able to find words in the grid forwards or backwards. 
7. FR 7 - The application must eliminate spaces from words in the wordsearch.

# Testing Proceedure
## Unit Testing
1. From root, run the following command from the terminal or command prompt:
    ```
    python -m  unittest test.test_alphabet_soup -v
    ```

## Integration Testing
2. From root, run the following command from the terminal or command prompt:
    ```
    python alphabet_soup/alphabet_soup.py test/test_input.txt
    ```

    NOTE: Two integration tests have been provided for this application:
    1. test_input.txt - Provides a test of the suggested input/output noted in this README.
    2. test_input2.txt - Provides a test of every directional search direction to validate application functionality.


# Run Application
    1. From root, run the following command from the terminal or command prompt:
    ```
    python alphabet_soup/alphabet_soup.py {crossword_input_file.txt}
    ```
