"""Context for testing imports"""
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from alphabet_soup import alphabet_soup
from alphabet_soup.word import Word
from alphabet_soup.grid import Grid
