"""Test Module"""
import unittest
from .context import alphabet_soup
from .context import Word
from .context import Grid

class TestAlphabetSoup(unittest.TestCase):
    """A Module for unit testing all application functions"""
    # Test Alphabet Soup functions
    def test_file_input(self):
        """
        -   Test input from file to ensure that it was loaded correctly.
        """
        self.assertEqual(
            alphabet_soup.file_input("test/test_input.txt"),
            "5x5\nH A S D F\nG E Y B H\nJ K L Z X\nC V B L N\nG O O D O\nHELLO\nGOOD\nBYE")

    def test_validate_dimensions(self):
        """
        -   Test validation of grid dimensions.
        """
        self.assertTrue(alphabet_soup.validate_dimensions("5x5"))
        self.assertTrue(alphabet_soup.validate_dimensions("25x2"))
        self.assertFalse(alphabet_soup.validate_dimensions("0x12"))
        self.assertFalse(alphabet_soup.validate_dimensions("10x0"))


    def test_parse_dimensions(self):
        """
        -   Test parsing of input dimensions.
        """
        rows, columns = alphabet_soup.parse_dimensions("10x12")
        self.assertEqual(rows, 10)
        self.assertEqual(columns, 12)

    def test_parse_grid(self):
        """
        -   Test parsing of file content into a grid matrix.
        """
        file_content = "5x5\nH A S D F\nG E Y B H\nJ K L Z X\nC V B L N\nG O O D O\nHELLO\nGOOD\nBYE"
        content_lines = file_content.split("\n")

        parsed_grid = [['H', 'A', 'S', 'D', 'F'], ['G', 'E', 'Y', 'B', 'H'],
                       ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N'],
                       ['G', 'O', 'O', 'D', 'O']]

        self.assertEqual(alphabet_soup.parse_grid(content_lines, 5), parsed_grid)

    def test_parse_wordlist(self):
        """
        -   Test parsing file content into string array for words.
        """
        file_content = "5x5\nH A S D F\nG E Y B H\nJ K L Z X\nC V B L N\nG O O D O\nHELLO\nGOOD\nBYE\nGOOD BYE"
        content_lines = file_content.split("\n")
        word_list = alphabet_soup.parse_wordlist(content_lines, 5, 10)
        self.assertEqual(word_list[0].word, "HELLO")
        self.assertEqual(word_list[1].word, "GOOD")
        self.assertEqual(word_list[2].word, "BYE")
        self.assertEqual(word_list[3].word, "GOODBYE")

    def test_search_grid(self):
        """
        -   Test search to find words in crossword grid.
        """
        grid = [['T', 'A', 'S', 'D', 'F'], ['G', 'S', 'Y', 'B', 'H'], 
                ['J', 'K', 'E', 'Z', 'X'], ['C', 'V', 'B', 'T', 'N'], 
                ['G', 'O', 'O', 'D', 'O']]
        word_list = [Word("TEST"), Word("GOOD")]
        rows = 5
        columns = 5

        alphabet_soup.search_grid(rows, columns, grid, word_list)

        self.assertEqual(str(word_list[0]), "TEST 3:3 0:0")
        self.assertEqual(str(word_list[1]), "GOOD 4:0 4:3")

    # WIP build test framework for search direction
    def test_search_direction(self):
        """
        -   Test searching for a word in a grid.
        """
        grid = [['T', 'A', 'S', 'D', 'F'], ['G', 'S', 'Y', 'B', 'H'], 
                ['J', 'K', 'E', 'Z', 'X'], ['C', 'V', 'B', 'T', 'N'], 
                ['G', 'O', 'O', 'D', 'O']]
        search_word = Word("TEST")
        start_row = 3
        start_column = 3
        row_increment = -1
        column_increment = -1
        word_index = 1

        alphabet_soup.search_direction(start_row, start_column,
                                       search_word, grid, row_increment, column_increment, 
                                       word_index)

        self.assertTrue(str(search_word), "TEST 3:3 0:0")

    # Test Word Functions
    def test_word_functions(self):
        """
        -   Test Constructor for the creation of a Word object and test associated functions.
        """
        test_word = Word("TESTING")

        self.assertEqual(test_word.word, "TESTING")
        self.assertEqual(test_word.length, 7)
        self.assertEqual(test_word.found, False)
        self.assertEqual(test_word.start, None)
        self.assertEqual(test_word.end, None)

        self.assertEqual(test_word.get_found(), False)
        test_word.set_found(True)
        self.assertEqual(test_word.get_found(), True)

        test_word.set_start(1, 3)
        self.assertEqual(test_word.start, "1:3")

        test_word.set_end(3, 8)
        self.assertEqual(test_word.end, "3:8")

        self.assertEqual(str(test_word), "TESTING 1:3 3:8")

    # Test Grid Functions
    def test_grid_functions(self):
        """
        -   Test Constructor and functions for crossword grid.
        """
        grid_matrix =[['H', 'A', 'S', 'D', 'F'], ['G', 'E', 'Y', 'B', 'H'], 
                      ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N']]
        test_grid = Grid(4, 5, grid_matrix)

        self.assertEqual(test_grid.row_dimension, 4)
        self.assertEqual(test_grid.column_dimension, 5)
        self.assertEqual(test_grid.grid[3][4], "N")

        rows, columns = test_grid.get_dimensions()
        self.assertEqual(rows, 4)
        self.assertEqual(columns, 5)

        self.assertEqual(test_grid.get_grid(), [
            ['H', 'A', 'S', 'D', 'F'], ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N']])

if __name__ == '__main__':
    unittest.main()
